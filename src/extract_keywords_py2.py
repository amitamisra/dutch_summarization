
'''
Created on Nov 6, 2017
read the data from xml files, use keyword extraction from gensim
@author: amita
'''
import sys
reload(sys)  
#sys.setdefaultencoding('Cp1252')
#print (sys.getdefaultencoding())
import file_utilities
import bs4
from bs4 import BeautifulSoup
from bs4.element import NavigableString
from bs4.element import Tag
import os
from googletrans import Translator
from stop_words import get_stop_words
import polyglot
from polyglot.text import Text, Word
from cucco import Cucco
import pandas as pd
#from summa import keywords
#import unicode
import RAKE
import io
import re
from rake_nltk import Rake
from summa import keywords as keywords_summa
stop_words_dutch = get_stop_words('dutch')
stop_words_en= get_stop_words('english')
num_chr=3
ngram=1
freq=7
ratio=0.2
ptn="dutch"



def translate_text(text):
    translator = Translator()
    engtext=translator.translate(text)
    return engtext

def keyword_summa(text,lang):
    text = re.sub(r"http\S+", "", text)
    keyword_list=keywords_summa.keywords(text,ratio=ratio,language=lang)
    #print(keyword_list)
    return keyword_list


def nltkrake(text,lang):
    r = Rake(language=lang)
    print(r.stopwords)
    print(text)
    r.extract_keywords_from_text(text,lang)
    keyword_list=r.get_ranked_phrases()
    print(keyword_list)
        

def keyword_rake(text,stop_words,num_chr,ngram,freq):
    print(type(text))
    text = re.sub(r"http\S+", "", text)
    cucco = Cucco()
    text=cucco.replace_punctuation(text)
    text=text.replace("\n"," ")
    #text=cucco.(text)
    #print(text)
    Rake = RAKE.Rake(stop_words,num_chr,ngram,freq)
    keywords=Rake.run(text)
    #print(keywords)
    return keywords

def merge_discussions(xml_doc,trans):
    alltexts=[]
    all_transtexts=[]
    with io.open(xml_doc,encoding="utf-8") as fp:
        soup = BeautifulSoup(fp)
        posts=soup.find_all('post')
        for post in posts:
            tags=post.contents
            for tag in tags:
                if tag.name=="body":
                    text=tag.get_text().encode("utf-8")
                    alltexts.append(text)
                    if trans==1:
                        trans_text=translate_text(text)
                        all_transtexts.append(trans_text.text)    

    return alltexts,all_transtexts
     
def get_xml_file_list(xml_dir):
    file_list=os.listdir(xml_dir)
    return file_list



def run(xml_dir,trans):
    allrows=[]
    file_list=get_xml_file_list(xml_dir)
    csv_file=os.path.join(data_dir+"106long20threads_keywords_ratio_"+str(ratio)+"pattern_"+ ptn+".csv")
    count=0
    for xml_file in file_list:
#         if count <46:
#              count=count+1
#              continue
        if "215636.xml" in xml_file:
            continue
        print(" processing filename {0}".format(xml_file))
        newdict={}
        file_path=os.path.join(xml_dir, xml_file)
        alltexts,all_transtexts=merge_discussions(file_path,trans)
        text_string= " ".join(alltexts)
        #keyword_dutch=nltkrake(text_string,"Dutch")
        keyword_dutch=keyword_summa(text_string,"dutch")
        #keyword_dutch=keyword_rake(text_string,stop_words_dutch,num_chr,ngram,freq)
        
        if trans==1:
            basedir=os.path.join(os.path.dirname(xml_dir),os.path.basename(xml_dir)+"text")
            if not os.path.exists(basedir):
                os.mkdir(basedir)
            outfile=os.path.join(basedir,os.path.basename(xml_file)[:-4]+".txt")
            trans_string=" ".join(all_transtexts)
            #keyword_english=keyword_rake(trans_string,stop_words_en,num_chr,ngram,freq)
            keyword_english=keyword_summa(trans_string,"english")
            #keyword_english=nltkrake(trans_string,"english")
            newdict["keyword_eng"]=keyword_english
            with io.open(outfile,"w",encoding="utf-8") as f:
                f.write(trans_string)
        newdict["keyword_ducth"]=keyword_dutch
        newdict["thread_id"]=os.path.basename(xml_file)
        #newdict["dutch_text"]=text_string
        newdict["ratio"]=ratio
        allrows.append(newdict)
        count=count+1
        print("Done processing a total {0} files ".format(count))
        if count==51:
             break
    print("Done processing a total {0} files ".format(count))    
    df=pd.DataFrame(allrows)
    df.to_csv(csv_file,encoding="utf-8",index=False)    
        
        
        
    
if __name__ == '__main__':
    data_dir=file_utilities.DATA_DIR
    xml_dir=os.path.join(data_dir,"106long20threads")
    trans=0
    run(xml_dir,trans)